#!/usr/bin/env python
"""
    This file is part of Graduation Audit System.
    Copyright (C) 2016 Saikiran Srirangapalli <saikiran1096@gmail.com>

    Graduation Audit System is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Graduation Audit System is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Graduation Audit System.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import print_function
import argparse
import util


def main():
    """
    This program writes writes <bs/ba><major><year>req2.lp.
    Pass in the degree major and year as command line arguments.
    Run as:
        python req_writer.py <degree_type> <major> <degree_year>
    ex: python req_writer.py bs cs 2015
    """
    parser = argparse.ArgumentParser('Writes <bs/ba><major><year>req2.lp')
    parser.add_argument('degree_type', choices=['bs', 'ba'])
    parser.add_argument('major')
    parser.add_argument('degree_year')

    args = parser.parse_args()

    degree, major, year = args.degree_type, args.major, args.degree_year

    directory = "%s/%s/%s/" % (degree, major, year)
    prefix = "%s%s" % (major, year)

    # Get list of courses all from course.lp
    courses = util.get_courses()

    # Get list of courses that could satisfy some requirement
    with open(directory + prefix + 'req.lp', 'r') as fil:
        req = fil.read()

    with open(directory + prefix + 'req2.lp', 'w') as fil:
        for course in courses:
            if not course in req:
                fil.write('_req(%s,none).\n' % course)

if __name__ == '__main__':
    main()
